package com.constantine.yuuh.utils;

import com.constantine.yuuh.Yuuh;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Setting {

    private Properties prop;

    public Setting(String path) {
        prop = new Properties();
        String propPath = path + "data/app.properties";
        try (InputStream is = new FileInputStream(propPath)) {
            prop.load(is);
            if (prop.getProperty("passwordHide") == null
                    || prop.getProperty("filePath") == null
                    || prop.getProperty("backupPath") == null
                    || prop.getProperty("keyPath") == null) {
                throw new IOException("invalid property");
            }
        } catch (IOException ex) {
            prop.setProperty("passwordHide", "******");
            prop.setProperty("filePath", path + "data/data.bin");
            prop.setProperty("backupPath", path + "data/");
            prop.setProperty("keyPath", path + "data/");
        }
        try (FileOutputStream output = new FileOutputStream(propPath)) {
            prop.store(output, null);
        } catch (IOException ex) {
            Logger.getLogger(Yuuh.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getFilePath() {
        return prop.getProperty("filePath");
    }

    public String getPasswordHide() {
        return prop.getProperty("passwordHide");
    }

    public String getBackupPath() {
        return prop.getProperty("backupPath");
    }

    public String getKeyPath() {
        return prop.getProperty("keyPath");
    }
}
