package com.constantine.yuuh.utils;

import com.constantine.yuuh.model.Account;
import com.constantine.yuuh.model.Info;
import com.constantine.yuuh.model.Data;
import java.io.*;
import java.util.*;

public class LocalStorage {

    private static volatile LocalStorage instance;

    public static LocalStorage getInstance() {
        if (instance == null) {
            synchronized (LocalStorage.class) {
                if (instance == null) {
                    instance = new LocalStorage();
                }
            }
        }
        return instance;
    }

    private LocalStorage() {
    }

    public Data load(String filePath) {
        Data result;
        try (InputStream file = new FileInputStream(filePath);
                InputStream buffer = new BufferedInputStream(file);
                ObjectInput input = new ObjectInputStream(buffer);) {
            Object object = input.readObject();
            try {
                result = (Data) object;
            } catch (ClassCastException e) {
                List<Account> accounts = (List<Account>) object;
                result = new Data();
                result.setAccounts(accounts);
            }
        } catch (ClassNotFoundException | IOException e) {
            List<Account> accounts = new ArrayList<>();
            accounts.add(new Account(1));
            result = new Data();
            result.setAccounts(accounts);
        }
        return result;
    }

    public Info save(Data data, String filePath) {
        Info info = new Info();
        try (OutputStream file = new FileOutputStream(filePath);
                OutputStream buffer = new BufferedOutputStream(file);
                ObjectOutput output = new ObjectOutputStream(buffer);) {
            output.writeObject(data);
            info.setTitle("Info");
            info.setMessage("Saved to local storage.");
        } catch (IOException ex) {
            info.setTitle("Error");
            info.setMessage("File not saved.");
            info.setSuccess(false);
        }
        return info;
    }

}
