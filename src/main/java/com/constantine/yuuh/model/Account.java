package com.constantine.yuuh.model;

import java.io.Serializable;

public class Account implements Serializable {

    private Integer id;
    private String target;
    private String login;
    private String password;
    private String description;

    public Account() {
    }

    public Account(Integer id) {
        this.id = id;
        target = login = password = description = "";
    }

    public Account(Integer id, String target, String login, String password, String description) {
        this.id = id;
        this.target = target;
        this.login = login;
        this.password = password;
        this.description = description;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }

    public Object[] toTableRow(String passwordHide) {
        return new Object[]{
            id,
            description,
            target,
            login,
            passwordHide
        };
    }

    @Override
    public String toString() {
        return "DATA[" + id + ", " + target + ", " + login + ", "
                + password + ", " + description + "]";
    }

}
