package com.constantine.yuuh.model;

public class Version {
    public static final int v1 = 1, //legacy 
            v2 = 2, // encode by master key
            v3 = 3;
}
