package com.constantine.yuuh.model;

public class Info {
    
    private boolean success = true;
    private String title;
    private String message;

    public Info() {
    }
    
    public Info(String title, String message) {
        this.title = title;
        this.message = message;
    }
    
    public Info(String title, String message, boolean success) {
        this(title, message);
        this.success = success;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public boolean isSuccess() {
        return success;
    }

    public void setSuccess(boolean success) {
        this.success = success;
    }
    
}
