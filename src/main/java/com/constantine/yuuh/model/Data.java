package com.constantine.yuuh.model;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

public class Data implements Serializable {
    
    private List<Account> accounts;
    private int version;
    private Date date;
    private String hash;

    public List<Account> getAccounts() {
        return accounts;
    }

    public void setAccounts(List<Account> accounts) {
        this.accounts = accounts;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public Date getDate() {
        return date;
    }

    public void setDate(Date date) {
        this.date = date;
    }

    public String getHash() {
        return hash;
    }

    public void setHash(String hash) {
        this.hash = hash;
    }
}
