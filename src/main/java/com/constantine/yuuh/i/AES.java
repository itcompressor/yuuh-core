package com.constantine.yuuh.i;

public interface AES {

    String encrypt(String data);

    String decrypt(String data);

    byte[] getKey();

    void setKey(byte[] key);
}
