package com.constantine.yuuh;

import com.constantine.yuuh.utils.LocalStorage;
import com.constantine.yuuh.utils.MD5;
import com.constantine.yuuh.model.Account;
import com.constantine.yuuh.model.Data;
import com.constantine.yuuh.model.Version;
import com.constantine.yuuh.i.AES;
import com.constantine.yuuh.utils.Setting;

public class Yuuh {

    private static volatile Yuuh instance;
    private String masterKey;

    private AES aes;
    private Setting setting;
    private LocalStorage localStorage;
    private Data data;

    public static Yuuh getInstance() {
        if (instance == null) {
            synchronized (Yuuh.class) {
                if (instance == null) {
                    instance = new Yuuh();
                }
            }
        }
        return instance;
    }

    public String getMasterKey() {
        return masterKey;
    }

    public void setMasterKey(String masterKey) {
        this.masterKey = masterKey;
    }

    public Data getData() {
        return data;
    }

    public void setData(Data data) {
        this.data = data;
    }

    public boolean init(AES aes, String appPath) {
        this.aes = aes;
        this.setting = new Setting(appPath);
        localStorage = LocalStorage.getInstance();
        return load();
    }

    public boolean load() {
        data = localStorage.load(setting.getFilePath());
        if (masterKey == null) {
            return false;
        }
        masterKey = MD5.getHash(masterKey);
        if (data.getVersion() >= Version.v2) {
            aes.setKey(masterKey.getBytes());
        }
        for (int i = 0; i < data.getAccounts().size(); i++) {
            Account account = data.getAccounts().get(i);
            account.setId(i + 1);
            account.setPassword(aes.decrypt(account.getPassword()));
        }
        data.setVersion(Version.v2);
        aes.setKey(masterKey.getBytes());
        if (data.getHash() == null) {
            data.setHash(aes.encrypt("yuuh"));
        }
        return aes.decrypt(data.getHash()).equals("yuuh");
    }

    public void save(Data data) {
        for (Account account : data.getAccounts()) {
            account.setPassword(aes.encrypt(account.getPassword()));
        }
        localStorage.save(data, setting.getFilePath());
    }
}
